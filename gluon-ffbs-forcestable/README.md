Force Stable Branch
===================

This Gluon package is intended to reset the branch of the Gluon updater to
`stable` after an update.

This can be used to switch routers that follow some other branch of a firmware
to the `stable` branch.

Why?
----
Inside the ffbs network we initially had two different flavours of firmware:

* One indented to be used inside Braunschwieg itself: SSID
  `braunschweig.freifunk.net`
* One intended to be used everywhere else: SSID: `Freifunk`

At one point we decided to switch to a single SSID `Freifunk` and thus have
two feature-identical branches again. This plugin was used to move the
remaining non-stable routers to the stable branch.
