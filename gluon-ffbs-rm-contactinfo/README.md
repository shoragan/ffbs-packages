Force Stable Branch
===================

This Gluon package removes contact info from uci after an update of the
router.

Freifunk Braunschweig will keep a non-public copy of the data given by
users to be able to contact someone.
